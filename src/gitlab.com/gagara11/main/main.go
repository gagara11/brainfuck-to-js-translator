package main

import (
	"gitlab.com/gagara11/translator"
	"io/ioutil"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	bfSources, readErr := ioutil.ReadFile(os.Args[1])
	check(readErr)

	result := translator.Translate(string(bfSources), os.Args[2], os.Args[4])

	writeErr := ioutil.WriteFile(os.Args[3], []byte(result), 0644)
	check(writeErr)
}
