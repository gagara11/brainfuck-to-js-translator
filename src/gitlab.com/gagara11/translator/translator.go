package translator

import (
	"bytes"
)

func Translate(src string, args string, arrayItemSize string) string {
	var res bytes.Buffer
	initJsEnv(&res, args, arrayItemSize)

	for i := 0; i < len(src); i++ {
		switch src[i] {
		case '+':
			res.WriteString("arr[i]++;")
			break
		case '-':
			res.WriteString("arr[i]--;")
			break
		case '>':
			res.WriteString("i++;")
			break
		case '<':
			res.WriteString("i--;")
			break
		case '.':
			res.WriteString("println(arr[i]);")
			break
		case ',':
			res.WriteString("arr[i] = getChar()")
			break
		case '[':
			res.WriteString("while(arr[i] > 0) {")
			break
		case ']':
			res.WriteString("}")
			break
		}
		res.WriteString("\n")
	}
	res.WriteString("console.log(output)")
	return res.String()
}

func initJsEnv(buff *bytes.Buffer, args string, arrayItemSize string) {
	buff.WriteString("var arr = new Uint" + arrayItemSize + "Array(60000); \n")
	buff.WriteString("var output = \"\"; \n")
	buff.WriteString("var argPos = 0; \n")
	buff.WriteString("var i = 0; \n")
	buff.WriteString("var args = \"" + args + "\"; \n")
	buff.WriteString("function getChar() {\n if (argPos >= args.length)\n return 10;\n var c = args.charCodeAt(argPos++); return c\n}")
	buff.WriteString("function println(c) { output += String.fromCharCode(c) }; \n")
}
